/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/* 
 * File:   main.cpp
 * Author: vishnu
 *
 * Created on 3 October, 2016, 11:59 PM
 */

#include <cstdlib>
#include <iostream>
#include <algorithm>
#include <limits>
#include <cmath>

using namespace std;

class doubleMatrix {
public:
    int rows, cols;
    double** matrix;
    doubleMatrix() {
        rows = 0;
        cols = 0;
        matrix = NULL;
    }
    doubleMatrix(int _rows, int _cols) {
        rows = _rows;
        cols = _cols;
        matrix = new double*[rows];
        for(int i=0; i<rows; i++) {
            matrix[i] = new double[cols];
        }
    }
    doubleMatrix(int _rows, int _cols, double** _matrix) {
        rows = _rows;
        cols = _cols;
        matrix = new double*[rows];
        for(int i=0; i<rows; i++) {
            matrix[i] = new double[cols];
        }
        for(int i=0; i<rows; i++) {
            for(int j=0; j<cols; j++) {
                matrix[i][j] = _matrix[i][j];
            }
        }
    }
    ~doubleMatrix() {
        for(int i = 0; i < rows; ++i) {
            delete [] matrix[i];
        }
        delete [] matrix;
    }
    void read() {
        int _rows, _cols;
        cin >> _rows >> _cols ; 
        
        rows = _rows;
        cols = _cols;
        matrix = new double*[rows];
        for(int i=0; i<rows; i++) {
            matrix[i] = new double[cols];
        }
        
        for(int i=0; i<rows; i++) {
            for(int j=0; j<cols; j++) {
                cin >> matrix[i][j] ;
            }
        }
    }
    void print() {
        cout << rows << " " << cols << " " ;
        for(int i=0; i<rows; i++) {
            for(int j=0; j<cols; j++) {
                cout << matrix[i][j] << " " ;
            }
        }
        cout << endl ;
    }
    void setZero() {
        for(int i = 0; i<rows; i++) {
            for(int j = 0; j<cols; j++) {
                matrix[i][j] = 0.0;
            }
        }
    }
};

/*
 * 
 */
int main(int argc, char** argv) {
    // Read A
    doubleMatrix A, B, P;
    A.read(); 
    
    // Read B
    B.read();
    
    // Read P (Initial state probabilities)
    P.read();
    
    int observationSequenceLength;
    cin >> observationSequenceLength;
    int observationSequence[observationSequenceLength];
    for(int i = 0; i < observationSequenceLength; i++) {
        cin >> observationSequence[i];
    }
    
    // initializations
    doubleMatrix alpha(observationSequenceLength, P.cols);
    doubleMatrix beta(observationSequenceLength, P.cols);
    double*** diGamma = new double**[observationSequenceLength];
    for(int i = 0; i < observationSequenceLength; i++) {
        diGamma[i] = new double*[A.rows];
        for(int j = 0; j<A.rows; j++) {
            diGamma[i][j] = new double[A.rows];
        }
    }
    doubleMatrix gamma(observationSequenceLength, A.cols);
    double old_probabilityObservationSequenceGivenLambda = -numeric_limits<double>::max();
    double* c = new double[observationSequenceLength];
    
    int iterCount = 0, iterLimit = 10000;
    bool stop = false;
    
    while(iterCount < iterLimit and !stop) {
        // Calculating alpha
        alpha.setZero();
        
        c[0] = 0.0;
        for(int i = 0; i<alpha.cols; i++) {
            alpha.matrix[0][i] = P.matrix[0][i] * B.matrix[i][observationSequence[0]];
            c[0] += alpha.matrix[0][i];
        }
        
        c[0] = 1.0 / c[0];
        for(int i = 0; i<alpha.cols; i++) {
            alpha.matrix[0][i] = c[0] * alpha.matrix[0][i];
        }
        
        for(int i = 1; i<alpha.rows; i++) {           // i loops through observations
            c[i] = 0;
            for(int j=0; j<alpha.cols; j++) {       // j loops through states
                alpha.matrix[i][j] = 0;
                for(int k=0; k<alpha.cols; k++) {   // k loops through states
                    alpha.matrix[i][j] += (alpha.matrix[i-1][k] * A.matrix[k][j]);
                }
                alpha.matrix[i][j] *= B.matrix[j][observationSequence[i]];
                c[i] += alpha.matrix[i][j];
            }
            c[i] = 1.0 / c[i];
            for(int j=0; j<alpha.cols; j++) {
                alpha.matrix[i][j] = c[i] * alpha.matrix[i][j];
            }
        }

        // Calculating beta
        beta.setZero();

        for(int i = 0; i<A.cols; i++) {
            beta.matrix[observationSequenceLength-1][i] = c[observationSequenceLength - 1];
        }

        for(int i = observationSequenceLength - 2; i>=0; i--) { // looping through time backwards
            for(int j = 0; j<A.cols; j++) {                  // looping through states
                beta.matrix[i][j] = 0;
                for(int k = 0; k<A.cols; k++) {              // looping through states
                    beta.matrix[i][j] += (A.matrix[j][k] * B.matrix[k][observationSequence[i+1]] * beta.matrix[i+1][k]);
                }
                beta.matrix[i][j] = c[i] * beta.matrix[i][j];
            }
        }
        
        gamma.setZero();
        
        // Calculating di-gamma and gamma
        for(int t = 0; t < observationSequenceLength - 1; t++) {
            double denom = 0;
            for(int i = 0; i < A.rows; i++) {
                for(int j = 0; j < A.rows; j++) {
                    denom +=
                            alpha.matrix[t][i]
                            * A.matrix[i][j]
                            * B.matrix[j][observationSequence[t+1]]
                            * beta.matrix[t+1][j];
                }
            }
            for(int i = 0; i < A.rows; i++) {
                for(int j = 0; j < A.rows; j++) {
                    diGamma[t][i][j] =
                            alpha.matrix[t][i]
                            * A.matrix[i][j]
                            * B.matrix[j][observationSequence[t+1]]
                            * beta.matrix[t+1][j]
                            / denom;
                    gamma.matrix[t][i] += diGamma[t][i][j];
                }
            }
        }
        
        double denom = 0;
        for(int i = 0; i < A.rows; i++) {
            denom += alpha.matrix[observationSequenceLength - 1][i];
        }
        for(int i = 0; i < A.rows; i++) {
            gamma.matrix[observationSequenceLength - 1][i] = alpha.matrix[observationSequenceLength - 1][i] / denom;
        }

        P.setZero();
        A.setZero();
        B.setZero();

        // recalculating P
        for(int i = 0; i < A.rows; i++) {
            for(int j = 0; j < A.rows; j++) {
                P.matrix[0][i] += diGamma[0][i][j];
            }
        }

        // recalculating A
        for(int i = 0; i < A.rows; i++) {
            for(int j = 0; j < A.cols; j++) {
                double numerator = 0, denominator = 0;
                for(int t = 0; t < observationSequenceLength - 1; t++) {
                    numerator += diGamma[t][i][j];
                    denominator += gamma.matrix[t][i];
                }
                if(denom != 0) {
                    A.matrix[i][j] = numerator / denominator;
                }
                else {
                    A.matrix[i][j] = 0;
                }
            }
        }

        // recalculating B
        for(int j = 0; j < B.rows; j++) {
            for(int k = 0; k < B.cols; k++) {
                double numerator = 0, denominator = 0;
                for(int t = 0; t < observationSequenceLength; t++) {
                    if(observationSequence[t]==k) {
                        numerator += gamma.matrix[t][j];
                    }
                    denominator += gamma.matrix[t][j];
                }
                if(denominator!=0) {
                    B.matrix[j][k] = numerator / denominator;
                }
                else {
                    B.matrix[j][k] = 0;
                }
            }
        }
        
        iterCount++;
        // loop exit conditions
        double logProbabilityObservationSequenceGivenLambda = 0.0;
        for(int i = 0; i<observationSequenceLength; i++) {
            logProbabilityObservationSequenceGivenLambda += log(c[i]);
        }
        logProbabilityObservationSequenceGivenLambda = -logProbabilityObservationSequenceGivenLambda;
        if(logProbabilityObservationSequenceGivenLambda > old_probabilityObservationSequenceGivenLambda) {
            old_probabilityObservationSequenceGivenLambda = logProbabilityObservationSequenceGivenLambda;
        }
        else {
            stop = true;
            break;
        }
    }
    A.print();
    B.print();
    // cerr << "iterCount: " <<  iterCount << endl ;
    return 0;
}

